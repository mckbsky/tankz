var searchData=
[
  ['render',['render',['../class_draw.html#aa70f9e9e0e137e45c291568d39a53951',1,'Draw::render(SDL_Renderer *r, SDL_Texture *t, SDL_Rect *rect)'],['../class_draw.html#a21c6d84e3065fdfafc58355ec9d70da6',1,'Draw::render(SDL_Renderer *r, SDL_Texture *t, SDL_Rect *rect_src, SDL_Rect *rect_dest)']]],
  ['render_5fex',['render_ex',['../class_draw.html#afe5766eec376e87eb1506190661ed066',1,'Draw']]],
  ['render_5fflipped',['render_flipped',['../class_draw.html#ab05950d8dc2faa79bb32e190e3bd9e6b',1,'Draw']]],
  ['render_5fstrech',['render_strech',['../class_draw.html#ad5426f73deb2b5f81149432f4c271f44',1,'Draw']]],
  ['return_5fto_5fmenu',['return_to_menu',['../class_event.html#aa0b299deb142042a4acd437edfc2328b',1,'Event']]]
];
