#include "draw.hpp"

SDL_Texture* Draw::load_texture_from_file(SDL_Renderer* r, std::string filename) {
	SDL_Texture *tmp_TEX;
	tmp_SURF = SDL_LoadBMP(filename.c_str());
	if(tmp_SURF == nullptr) {
		SDL_Log("%s", SDL_GetError());
	}
	SDL_SetColorKey(tmp_SURF, SDL_TRUE, Draw::COLOR_KEY);
	tmp_TEX = SDL_CreateTextureFromSurface(r, tmp_SURF);
	if (tmp_TEX == nullptr) {
		SDL_Log("%s", SDL_GetError());
	}
	SDL_SetTextureBlendMode(tmp_TEX, SDL_BLENDMODE_BLEND);
	SDL_FreeSurface(tmp_SURF);

	return tmp_TEX;
}

SDL_Texture* Draw::load_texture_from_file(SDL_Renderer *r, SDL_Rect rect, std::string filename) {
	SDL_Texture *tmp_TEX;
	tmp_SURF = SDL_LoadBMP(filename.c_str());
	if (tmp_SURF == nullptr) {
		SDL_Log("%s", SDL_GetError());
	}
	tmp_TEX = SDL_CreateTexture(r, SDL_PIXELFORMAT_ABGR32, SDL_TEXTUREACCESS_STREAMING, rect.w, rect.h);
	if (tmp_TEX == nullptr) {
		SDL_Log("%s", SDL_GetError());
	}
	SDL_SetTextureBlendMode(tmp_TEX, SDL_BLENDMODE_BLEND);
	SDL_UpdateTexture(tmp_TEX, NULL, tmp_SURF->pixels, rect.w * sizeof(Uint32));
	SDL_FreeSurface(tmp_SURF);

	return tmp_TEX;
}

SDL_Surface* Draw::load_texture_from_file(SDL_Renderer *r, SDL_Texture **t, SDL_Rect rect, std::string filename) {
	SDL_Surface *s;
	s = SDL_LoadBMP(filename.c_str());
	if (s == nullptr) {
		SDL_Log("%s", SDL_GetError());
	}
	*t = SDL_CreateTexture(r, SDL_PIXELFORMAT_ABGR32, SDL_TEXTUREACCESS_STREAMING, rect.w, rect.h);
	if (*t == nullptr) {
		SDL_Log("%s", SDL_GetError());
	}
	SDL_SetTextureBlendMode(*t, SDL_BLENDMODE_BLEND);
	SDL_UpdateTexture(*t, NULL, s->pixels, rect.w * sizeof(Uint32));

	return s;
}

void Draw::render(SDL_Renderer *r, SDL_Texture *t, SDL_Rect *rect) {
  SDL_RenderCopy(r, t, nullptr, rect);
}

void Draw::render(SDL_Renderer *r, SDL_Texture *t, SDL_Rect rect, int x_offset, int y_offset) {
	rect.x += x_offset;
	rect.y += y_offset;
	SDL_RenderCopy(r, t, nullptr, &rect);
}

void Draw::render(SDL_Renderer *r, SDL_Texture *t, SDL_Rect *rect_src, SDL_Rect *rect_dest) {
  SDL_RenderCopy(r, t, rect_src, rect_dest);
}

void Draw::render_ex(SDL_Renderer *r, SDL_Texture *t, SDL_Rect *rect, SDL_Point point, double angle) {
	SDL_RenderCopyEx(r, t, nullptr, rect, -angle, &point, SDL_FLIP_NONE);
}

void Draw::render_flipped(SDL_Renderer *r, SDL_Texture* t, SDL_Rect *rect, SDL_Point *point, double angle) {
	SDL_RenderCopyEx(r, t, nullptr, rect, angle, point, SDL_FLIP_HORIZONTAL);
}

void Draw::render_strech(SDL_Renderer *r, SDL_Texture *t, SDL_Rect rect, int w, int h) {
	rect.w = w;
	rect.h = h;
  SDL_RenderCopy(r, t, nullptr, &rect);
}

SDL_Surface* Draw::tmp_SURF = nullptr;
const int Draw::COLOR_KEY = 0xff00ff;
