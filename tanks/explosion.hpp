#ifndef EXPLOSION_HPP
#define EXPLOSION_HPP

#include "header.hpp"
#include <iostream>

/*!
 * \class Explosion
 * \brief Klasa reprezentująca animację eksplozji
 */

class Explosion {
private:
  int interval;         //!< czas między klatkami animacji
  int time_check;       //!< licznik czasu
  int anim_rows;        //!< ilośc rzędów animacji
  int anim_cols;        //!< ilość kolumn animacji
  int i;                //!< licznik rzędów
  int j;                //!< licznik kolumn
  bool stop;            //!< informacja o zakończeniu animacji
  bool start_timer;     //!< informacja o rozpoczęciu odliczania
  SDL_Texture *explosion_TEX;
  SDL_Rect explosion_RECT;
  SDL_Rect explosion_part_RECT;
  SDL_Renderer *r;

public:
  /*!
   * \fn Explosion(SDL_Renderer *_r, int type, SDL_Point p)
   * \param SDL_Renderer* wskaźnik do struktury renderera umożliwiającej rysowanie
   * \param int rodzaj animacji explozji (0 - wybuch, 1 - wystrzał)
   * \param SDL_Point punkt startowy animacji
   * \brief Konstruktor klasy Explosion
   */
  Explosion(SDL_Renderer *_r, int type, SDL_Point p);

  /*!
   * \fn ~Explosion(void)
   * \brief Desktuktor klasy Explosion
   *
   * Niszczy tekstury związane z obiektem
   */
  ~Explosion(void);

  /*!
   * \fn void draw(void)
   * \brief funkcja animująca
   *
   * Rysuje część tekstury animacji z ustalonym odstępem czasowym między
   * klatkami, aż do narysowania wszystkich. Metoda ustawia wtedy zmienną
   * stop na true i nie pozwala dalej rysować;
   */
  void draw(void);
};

#endif //EXPLOSION_hpp
