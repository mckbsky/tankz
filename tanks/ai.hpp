#ifndef AI_HPP
#define AI_HPP

#include "tank.hpp"
#include <iostream>
#include <ctime>

/*!
 * \class Ai
 * \brief Klasa obsługująca sztuczną inteligencję
 */

class Ai: public Tank {
private:
  SDL_Point oppenent_pos;   //!< Pozycja (x, y) przeciwnika
  SDL_Point collision_pos;  //!< Pozycja (x, y) ostatniego trafienia

  bool ai_first_call;       //!< Zmienna resetująca licznik
  int ai_time_check;        //!< Opóźnienie wystrzału
  int ai_last_turn;         //!< Zdarzenie z ostatniej tury

public:
  /*!
   * \fn Ai(SDL_Renderer *_r, int _model, int _position, SDL_Point tank_position, SDL_Point _oppenent_pos)
   * \brief Konstruktor klasy Ai
   *
   * \param SDL_Renderer* wskaźnik do struktury renderera umożliwiającej rysowanie
   * \param int numer modelu czołgu do załadowania
   * \param int pozycja czołgu na mapie (lewa, prawa)
   * \param SDL_Point pozycja czołgu (x, y) na mapie
   * \param SDL_Point pozycja przeciwnika (x, y) na mapie
   *
   * Metoda wywołuje konstruktor nadrzędnej funkcji Tank
   */
  Ai(SDL_Renderer *_r, int _model, int _position, SDL_Point tank_position, SDL_Point _oppenent_pos);

  /*!
   * \fn void ai_analize(SDL_Point p)
   * \brief Metoda analizująca poprzednią turę
   *
   * \param SDL_Point punkt (x, y) kolizji pocisku rundzie
   *
   * Metoda analizuje kolizję pocisku w rundzie uwzględniając np: położenie przeciwnika,
   * odległość od celu na różnych płaszczyznach, występowanie strzałów w to samo miejsce itp,
   * po czym ustawia zmienną ai_last_turn w celu wykonania przez metodę ai_turn()
   * odpowiedniej akcji.
   */
  void ai_analize(SDL_Point p);

  /*!
   * \fn bool ai_turn()
   * \brief Wykonanie akcji zadanej przez ai_analize()
   *
   * Po odczekaniu 1,2s wywołuje akcję zgodnie ze stanem zmiennej ai_last_turn
   */
  bool ai_turn(void);

  /*!
   * \fn bool is_ai(void)
   * \brief Sprawdza czy czołg jest obsługiwany przez komputer
   *
   * \return dla obiektu klasy Ai zwraca true
   */
  bool is_ai(void);
};

#endif //AI_HPP
