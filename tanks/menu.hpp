#ifndef MENU_HPP
#define MENU_HPP
#include <string>
#include <vector>
#include "header.hpp"
#include "window.hpp"

/*!
 * \enum menu_item
 * \brief Stany występujące w głównym menu
 */
typedef enum {new_game, options, authors, exit_game, title} menu_item;


/*!
 * \class Menu
 * \brief Klasa reprezentująca główne menu
 */
class Menu {
private:
	SDL_Color color;
	SDL_Renderer *r;
  SDL_Texture *background_TEX;
	SDL_Texture *ui1_pad_TEX;
	SDL_Texture *ui2_pad_TEX;
	SDL_Texture *ui1_kb_TEX;
	SDL_Texture *ui2_kb_TEX;
	SDL_Texture *menu_TEXT;
	SDL_Texture *fill_TEX;
	SDL_Texture *main_menu_TEX[4];
	SDL_Texture *new_game_TEX[6];
	SDL_Texture *authors_TEX[2];
	SDL_Texture *options_TEX[2];
  SDL_Rect menu_RECT;
	SDL_Rect main_menu_RECT[4];
	SDL_Rect new_game_RECT[6];
	SDL_Rect authors_RECT[2];
	SDL_Rect options_RECT[2];
	SDL_Rect menu_text_RECT;
	SDL_Rect ui_RECT;
	SDL_Surface *tmp_SURF;
	TTF_Font *font;

	short state;											//!< Stan w menu związany ze zmienną enum menu_item
	short position[10];								//!< Pozycja dla danego stanu
	int volume;												//!< Glośność muzyki i dźwięków
	const int BUTTON_HEIGHT = 63;			//!< Wysokość przycisku
	const int BUTTON_WIDTH = 817;			//!< Szerokość przycisku
	const int BUTTON_X = 563;					//!< Współrzędna x przycisku
	const int BUTTON_Y = 400;					//!< Współrzędna y przycisku
	int new_game_V[4];								//!< Wybrane wartości w menu nowej gry
	Mix_Chunk *menu_SOUND;						//!< Dźwięk przycisków w menu
	std::vector<std::string> new_game_MAPPED_V[4];	//!< Tablica wektrów stringów zawierająca słowne wartości z menu nowej gry

	/*!
	 * \fn void init_submenu_rect(SDL_Rect *rect, int n)
	 * \param SDL_Rect* Tablica prostokątów do zainicjowania
	 * \param int rozmiar tablicy
	 * \brief Ustawia pozycje przycisków w menu
	 */
	void init_submenu_rect(SDL_Rect *rect, int n);

	/*!
	 * \fn SDL_Texture* set_menu_text(std::string s, SDL_Rect &rect)
	 * \param std::string Tekst do wpisania na przycisk
	 * \param SDL_Rect& Rozmiar i pozycja przycisku
	 * \return tekstura przycisku
	 * \brief Wpisuje tekst na przycisk
	 */
	SDL_Texture* set_menu_text(std::string s, SDL_Rect &rect);


public:
	int ui;									//!< Rodzaj interfejsu (klawiatura, gamepad)
	Mix_Music *bgm_MUSIC;		//!< Muzyka w menu

	/*!
	 * \fn Menu(SDL_Renderer *_r);
	 * \param SDL_Renderer* wskaźnik do struktury renderera umożliwiającej rysowanie
	 * \brief Kostruktor klasy Menu
	 *
	 * Metoda ustawia teksty wszystkich przycisków oraz ładuje dźwięki i tekstury
	 * interfejsu
	 */
  Menu(SDL_Renderer *_r);

	/*!
	 * \fn ~Menu(void)
	 * \brief Desktruktor klasy Menu
	 *
	 * Niszczy tekstury i powierzchnie związane z głównym menu
	 */
	~Menu(void);

	/*!
	 * \fn void draw(void)
	 * \brief Metoda rysująca główne menu, przycisku, interfejs
	 */
	void draw(void);

	/*!
	 * \fn int get_value(int n)
	 * \param int Pozycja w podmenu nowej gry
	 * \return Wartość danej opcji w podmenu nowej gry
	 * \brief Wartość pozycji w podmenu nowej gry np: model czolgu, mapa
	 */
	int get_value(int n);

	/*!
	 * \fn short get_position(void)
	 * \return Pozycja w danym podmenu
	 */
	short get_position(void);

	/*!
	 * \fn void next_item(void)
	 * \brief Zmienia pozycję w menu na kolejną
	 */
  void next_item(void);

	/*!
	 * \fn void next_item(void)
	 * \brief Zmienia pozycję w menu na poprzednią
	 */
  void previous_item(void);

	/*!
	 * \fn short get_state(void)
	 * \return stan w menu
	 */
	short get_state(void);

	/*!
	 * \fn void set_state(short)
	 * \param short stan w menu
	 * \brief ustawia stan
	 */
	void set_state(short);

	/*!
	 * \fn void void increase_property(void)
	 * \brief Zwiększa obecnie zaznaczoną opcję
	 */
	void increase_property(void);

	/*!
	 * \fn void decrease_property(void)
	 * \brief Zmniejsza obecnie zaznaczoną opcję
	 */
	void decrease_property(void);

	/*!
	 * \fn void set_volume(int v);
	 * \param int Głośność
	 * \brief Ustawia głośność na zadaną wartość
	 */
	void set_volume(int v);

	/*!
	 * \fn void decrease_property(void)
	 * \brief Zmienia treść przycisków w danym submenu
	 */
	void update_submenu(void);
};

#endif
