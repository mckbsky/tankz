#ifndef BULLET_HPP
#define BULLET_HPP

#include "header.hpp"

/*!
 * \class Bullet
 * \brief Klasa reprezentujaca pocisk
 */

class Bullet{
private:
	double x0;							//!< Współrzędna początkowa x
	double y0;							//!< Współrzędna początkowa y
	double v0;							//!< Prędkość początkowa
	double tang;						//!< Tangens kąta alfa
	double t;								//!< Czas
	double interval;				//!< Stała potrzeba przy wyliczaniu współrzędnej x w rzucie ukośnym
	double denominator;			//!< Stały mianownik we wzorze na współrzędną y w rzucie ukośnym
	int position;						//!< Pozycja strzelającego czołgu (lewa, prawa)
  int wind;								//!< Siła wiatru na mapie

  SDL_Texture *arrow_TEX;
  SDL_Texture *bullet_TEX;
  SDL_Rect arrow_RECT;
  SDL_Rect bullet_RECT;
	SDL_Renderer *r;

public:
	/*!
	 * \fn Bullet(SDL_Renderer *_r, SDL_Point barrel_tip, double _v0, double angle_RAD, int _position, int _wind)
	 * \brief Konstruktor klasy Bullet
	 *
	 * \param SDL_Renderer* wskaźnik do struktury renderera umożliwiającej rysowanie
	 * \param SDL_Point pozycja startowa (x, y) pocisku
	 * \param double prędkość początkowa pocisku
	 * \param double kąt nachylenia lufy w radianach
	 * \param int pozycja czołgu (lewa, prawa)
	 * \param int wartość siły wiatru
	 */
  Bullet(SDL_Renderer *_r, SDL_Point barrel_tip, double _v0, double angle_RAD, int _position, int _wind);

	/*!
	 * \fn ~Bullet(void)
	 * \brief deskturkor klasy Bullet
	 *
	 * Niszczy związane z klasą tekstury
	 */
	~Bullet(void);

	/*!
	 * \fn void draw(void)
	 * \brief rysowanie pocisku
	 *
	 * Metoda rysuje pocisk oraz strzałkę ułatwiającą jego lokalizację gdy współrzędna y pocisku ma wartość poniżej 0
	 */
  void draw(void);

	/*!
	 * \fn SDL_Rect *get_collision(void)
	 * \brief Pobiera kolizję pocisku
	 * \return Prostokąt kolizji pocisku (x, y, w, h)
	 */
	SDL_Rect *get_collision(void);

	/*!
	 * \fn void update_trajectory(void)
	 * \brief Aktualizacja pozycji trajektorii
	 *
	 * Metoda wylicza współrzędne x, y pocisku dla danej klatki korzystając ze wzoru na rzut ukośny.
	 * Współrzędne są liczone w zależności od pozycji czołgu (lewa, prawa)
	 * http://www.fizykon.org/kinematyka/rzuty_rzut_ukosny.htm
	 */
	void update_trajectory(void);
};

#endif //BULLET_HPP
