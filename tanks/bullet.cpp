#include "bullet.hpp"


Bullet::Bullet(SDL_Renderer *_r, SDL_Point barrel_tip, double _v0, double angle, int _position, int _wind) {
	//initialize members
	position = _position;
	r = _r;
	v0 = _v0;
	t = 0.1;
	wind = _wind;
	tang = tan(angle);
	interval = (v0 + wind) * cos(angle);
	denominator = 2 * pow(v0, 2) * pow(cos(angle), 2);

	//initialize sdl_rectangles
	bullet_RECT.x = x0 = barrel_tip.x, bullet_RECT.y = y0 = barrel_tip.y, bullet_RECT.w = 8, bullet_RECT.h = 8;
	arrow_RECT.x = 0, arrow_RECT.y = 0, arrow_RECT.w = arrow_RECT.h = 50;

	//load textures
	bullet_TEX = Draw::load_texture_from_file(r, bullet_RECT, "images/bullet.bmp");
	arrow_TEX = Draw::load_texture_from_file(r, arrow_RECT, "images/arrow.bmp");
}

Bullet::~Bullet() {
	SDL_DestroyTexture(bullet_TEX);
	SDL_DestroyTexture(arrow_TEX);
}

void Bullet::draw(void) {
	Draw::render(r, bullet_TEX, &bullet_RECT);
	if (bullet_RECT.y < 0) {
		arrow_RECT.x = bullet_RECT.x - 25;
		Draw::render(r, arrow_TEX, &arrow_RECT);
	}
}

SDL_Rect* Bullet::get_collision(void) {
	return &bullet_RECT;
}

void Bullet::update_trajectory(void) {
	if (position == 0) {
		bullet_RECT.x += interval * t;
		bullet_RECT.y = y0 + (bullet_RECT.x - x0) * tang + (10 * pow(bullet_RECT.x - x0, 2)) / denominator;
	}
	else {
		bullet_RECT.x -= interval * t;
		bullet_RECT.y = y0 - (bullet_RECT.x - x0) * tang + (10 * pow(bullet_RECT.x - x0, 2)) / denominator;
	}
	t += 0.0002;
}
