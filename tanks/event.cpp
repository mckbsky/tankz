#include "event.hpp"

Event::Event(SDL_Renderer *_r) {
  //initialize members
  color = { 255, 255, 255 };
  first_press = false;
  r = _r;
  game_quit = false;
	block_input = false;
  ai_turn = false;

  state = main_menu;
  winner_TEX = nullptr;
  shoot_SOUND = Mix_LoadWAV("sounds/fire.wav");
  explosion_SOUND = Mix_LoadWAV("sounds/explosion.wav");

  //initialize object members
  bullet = nullptr;
  current_tank = nullptr;
  tank1 = nullptr;
  tank2 = nullptr;
  explosion1 = nullptr;
  explosion2 = nullptr;

  //create main menu
  main_event = new SDL_Event;
  menu = new Menu(r);

	//initialize fonts
	font = TTF_OpenFont("PixelFJVerdana12pt.ttf", 17);
	if (font == nullptr) {
		SDL_Log("%s", TTF_GetError());
	}
  winner_RECT.x = 760, winner_RECT.y = 440; winner_RECT.w = 400, winner_RECT.h = 100;
	turn_RECT = {0, 0, 0, 0};
	pixel_RECT.w = pixel_RECT.h = 1;
  bcg_RECT.x = 0, bcg_RECT.y = 0, bcg_RECT.w = 1920, bcg_RECT.h = 90;
}

void Event::set_up_wind(void) {
  wind_RECT = {0, 0, 0, 0};
  int wind_V = stage->get_wind();
  if(wind_V == 0) {
    wind = "Bezwietrznie";
  }
  else if(wind_V < -10) {
    wind = "Silny wiart wschodni";
  }
  else if(wind_V < 0) {
    wind = "Lekki wiart wschodni";
  }
  else if(wind_V < 10) {
    wind = "Lekki wiart zachodni";
  }
  else {
    wind = "Silny wiatr zachodni";
  }
  SDL_Color blue = {21, 187, 212};
  tmp_SURF = TTF_RenderText_Solid(font, wind.c_str(), blue);
  wind_TEXT = SDL_CreateTextureFromSurface(this->r, tmp_SURF);
  SDL_QueryTexture(wind_TEXT, NULL, NULL, &wind_RECT.w, &wind_RECT.h);
}

Event::~Event(void) {
  delete main_event;
  if(winner_TEX) {
    SDL_DestroyTexture(winner_TEX);
    winner_TEX = nullptr;
  }
  if(explosion1) {
    delete explosion1;
  }
	if (menu) {
		delete menu;
	}
}

void Event::set_turn(void) {
	if (current_tank == tank2 || current_tank == nullptr) {
		turn = "Tura gracza 1";
		current_tank = tank1;
	}
	else {
		turn = "Tura gracza 2";
		current_tank = tank2;
	}
	if(current_tank->is_ai()) {
		turn = "Tura CPU";
    ai_turn = true;
    block_input = true;
	}
  else {
    ai_turn = false;
    block_input = false;
  }
	tmp_SURF = TTF_RenderText_Solid(font, turn.c_str(), color);
	turn_TEXT = SDL_CreateTextureFromSurface(this->r, tmp_SURF);
  SDL_QueryTexture(turn_TEXT, NULL, NULL, &turn_RECT.w, &turn_RECT.h);
}

void Event::cpu_turn(void) {
  if(ai_turn == false) {
    return;
  }
  Ai& ai_tank = dynamic_cast<Ai&>(*current_tank);
  if(ai_tank.ai_turn()) {
    ai_turn = false;
    shoot();
  }
}

void Event::draw() {
  if(state == main_menu) {
    menu->draw();
  }
  else if(state == game) {
    stage->draw();
    SDL_RenderFillRect(r, &bcg_RECT);
    if(explosion1) {
      explosion1->draw();
    }
    if(explosion2) {
      explosion2->draw();
    }
    tank1->draw();
    tank2->draw();
		if(bullet) {
			if(check_collision() == false) {
        bullet->draw();
        bullet->update_trajectory();
      }
			else {
        end_turn();
			}
		}

		Draw::render(r, this->turn_TEXT, this->turn_RECT, 960 - turn_RECT.w / 2, 10);
    Draw::render(r, this->wind_TEXT, this->wind_RECT, 960 - wind_RECT.w / 2, 50);

		if (winner_TEX) {
			Draw::render(r, this->winner_TEX, &this->winner_RECT);
			SDL_RenderPresent(r);
			SDL_Delay(2500);
      return_to_menu();
		}
  }
}

void Event::end_turn(void) {
	delete bullet;
	bullet = nullptr;
}

bool Event::check_collision(void) {
  if(check_collision(bullet)) {
    if(current_tank->is_ai()) {
      Ai& ai_tank = dynamic_cast<Ai&>(*current_tank);
      ai_tank.ai_analize({bullet->get_collision()->x, bullet->get_collision()->y});
    }
    set_turn();

    return true;
  }
  else if(check_collision(bullet, stage)) {
    if(explosion1) {
      delete explosion1;
      explosion1 = nullptr;
    }
    explosion1 = new Explosion(r, 0, {bullet->get_collision()->x, bullet->get_collision()->y});
    destroy_stage(bullet, stage, 40);
    Mix_PlayChannel(-1, explosion_SOUND, 0);

    if(current_tank->is_ai()) {
      Ai& ai_tank = dynamic_cast<Ai&>(*current_tank);
      ai_tank.ai_analize({bullet->get_collision()->x, bullet->get_collision()->y});
    }
    set_turn();

    return true;
  }
  else if (check_collision(bullet, tank1)) {
    if(current_tank->is_ai()) {
      winner_TEX = Draw::load_texture_from_file(r, winner_RECT, "images/winnerCPU.bmp");
    }
    else {
      winner_TEX = Draw::load_texture_from_file(r, "images/winner2.bmp");
    }
  }
  else if (check_collision(bullet, tank2)) {
    winner_TEX = Draw::load_texture_from_file(r, winner_RECT, "images/winner1.bmp");
  }
  return false;
}

bool Event::check_collision(Bullet *b) {
	tmp_RECT = b->get_collision();
	if (tmp_RECT->x < 0 || tmp_RECT->x > 1920 || tmp_RECT->y > 1080) {
		return true;
	}
	return false;
}

template <typename T1, typename T2>
bool Event::check_collision(T1 *t1, T2 *t2) {
  SDL_Rect result;
	if (SDL_IntersectRect(t1->get_collision(), t2->get_collision(), &result) == SDL_TRUE) {
    if(SDL_RectEquals(t1->get_collision(), &result) == SDL_FALSE) {
      return false;
    }
    else {
      Uint32 pixel = *get_pixel(t2->get_surface(), t1->get_collision()->x, t1->get_collision()->y - t2->get_collision()->y);
      if(pixel == 0xffffff00) {
        return false;
      }
    }
    return true;
	}
	return false;
}

bool Event::enter_menu() {
  if(menu->get_state() == title) {
    switch(menu->get_position()) {
      case new_game: {
        menu->set_state(new_game);
        break;
      }
      case options: {
        menu->set_state(options);
        break;
      }
      case authors: {
        menu->set_state(authors);
        break;
      }
      case exit_game: {
        quit();
      }
      default: {
        break;
      }
    }
  }
  else if(menu->get_state() == new_game) {
    if(menu->get_position() == 4) {
      stage = new Stage(r, menu->get_value(1), menu->get_value(3));
      set_up_wind();
      state = game;
      tank1 = new Tank(r, menu->get_value(0), 0, stage->get_left_tank_position());
      if(menu->get_value(2)) {
        tank2 = new Ai(r, menu->get_value(0), 1, stage->get_right_tank_position(), {tank1->get_collision()->x, tank1->get_collision()->y});
      }
      else {
        tank2 = new Tank(r, menu->get_value(0), 1, stage->get_right_tank_position());
      }
			set_turn();
    	if(Mix_PlayingMusic() == 1) {
    		Mix_HaltMusic();
    	}
      return true;
    }
    else if(menu->get_position() == 5) {
      menu->set_state(title);
    }
  }
  else if(menu->get_state() == authors) {
    if(menu->get_position() == 1) {
      menu->set_state(title);
    }
  }
  else if(menu->get_state() == options) {
    if(menu->get_position() == 1) {
      menu->set_state(title);
    }
  }

  return false;
}

void Event::shoot(void) {
  if(explosion2) {
    delete explosion2;
  }
  explosion2 = new Explosion(r, 1, current_tank->get_barrel_tip());
	bullet = new Bullet(r, current_tank->get_barrel_tip(), current_tank->get_power(), current_tank->get_angle(), current_tank->get_position(), current_tank == tank1 ? stage->get_wind() : -stage->get_wind());
	block_input = true;
	Mix_PlayChannel(-1, shoot_SOUND, 0);
}

void Event::return_to_menu(void) {
  if(bullet) {
    delete bullet;
  }
  delete tank1;
  delete tank2;
  delete stage;

  if(winner_TEX) {
		SDL_DestroyTexture(winner_TEX);
		winner_TEX = nullptr;
  }

  bullet = nullptr;
  tank1 = nullptr;
  tank2 = nullptr;
  current_tank = nullptr;
  stage = nullptr;
  state = main_menu;
  menu->set_state(title);
  Mix_PlayMusic(menu->bgm_MUSIC, -1);
}

void Event::destroy_stage(Bullet *b, Stage *s, int r) {
  int x = b->get_collision()->x;
  int y = b->get_collision()->y - s->get_collision()->y;

  SDL_LockSurface(s->get_surface());

  for(int i = x - r; i <= x + r; ++i) {
    for(int j = y - r; j <= y + r; ++j) {
      if(i >= 0 && i <= s->get_collision()->w && j >= 0 && j <= s->get_collision()->h) {
        if(pow(r, 2) >= pow(x - i, 2) + pow(y - j, 2)) {
          *get_pixel(s->get_surface(), i, j) = 0xffffff00;
        }
      }
    }
  }

  SDL_UnlockSurface(s->get_surface());
  SDL_UpdateTexture(s->get_texture(), NULL, s->get_surface()->pixels, s->get_collision()->w * sizeof(Uint32));
}

Uint32* Event::get_pixel(SDL_Surface *surface, int x, int y) {
  int bpp = surface->format->BytesPerPixel;
  Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;
  return (Uint32 *)p;
}

void Event::quit(void) {
  main_event->type = SDL_QUIT;
  game_quit = true;
}
