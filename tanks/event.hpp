#ifndef EVENT_HPP
#define EVENT_HPP

#include "header.hpp"
#include "menu.hpp"
#include "stage.hpp"
#include "tank.hpp"
#include "ai.hpp"
#include "bullet.hpp"
#include "explosion.hpp"
#include <string>
#include <iostream>

class Menu;
class Stage;
class Tank;
class Bullet;
class Explosion;
class Ai;

/*!
 * \enum game_state
 * \brief Enum z wartościami stanu gry (menu, gra)
 */
enum game_state {main_menu, game};

/*!
 * \class Event
 * \brief Główna klasa zarządzająca zdarzeniami i większością obiektów
 */

class Event {
private:
  SDL_Renderer *r;
	SDL_Surface* tmp_SURF;
	SDL_Texture* turn_TEXT;
  SDL_Texture* wind_TEXT;
  SDL_Rect turn_RECT;
  SDL_Rect wind_RECT;
	SDL_Rect *tmp_RECT;
  SDL_Rect bcg_RECT;
	SDL_Texture *winner_TEX;
	SDL_Rect winner_RECT;
	TTF_Font* font;
	SDL_Color color;
  SDL_Rect pixel_RECT;

	std::string turn;                //!< Tekst informujący o aktualnej turze
  std::string wind;                //!< Tekst informujący o sile i kierunku wiatru
  Mix_Chunk *shoot_SOUND;          //!< Dźwięk wystrzału
  Mix_Chunk *explosion_SOUND;      //!< Dźwięk eksplozji


  /*!
   * \fn void set_up_wind(void)
   * \brief Ustawienie informacji o wietrze w zależności od jego siły
   */
  void set_up_wind(void);

  /*!
   * \fn void end_turn(void)
   * \brief Kończy turę
   */
  void end_turn(void);

  /*!
   * \fn bool check_collision(void);
   * \return true w przypadku wystąpienia kolizji
   * \brief Sprawdzanie kolizji
   *
   * Metoda wywołuje inne przeciążone metody check_collision() do sprawdzania
   * czy nie nastąpiła kolizja. W przypadku wystąpienia kolizji z czołgiem
   * metoda ładuje teksturę z informacją o zwycięzcy. W razie kolizji z otoczeniem
   * i o ile obecna tura to tura CPU wywoływana jest metoda ai_analize obiektu
   * klasy Ai. Oprócz tego metoda animację i dzwięk eksplozji.
   */
  bool check_collision(void);

  /*!
   * \fn bool check_collision(Bullet *b);
   * \return true w przypadku wystąpienia kolizji
   * \brief Sprawdzanie kolizji pocisku z krawędzią ekranu
   *
   * Metoda sprawdza czy wartości x i y pocisku znajdują się w zakresach
   * odpowiednio: <0, 1920> i y < 1080
   */
	bool check_collision(Bullet *b);

  /*!
  * \fn bool check_collision(Bullet *b);
  * \param typename obiekt 1 z metodami obsługi kolizji
  * \param typename obiekt 2 z metodami obsługi kolizji
  * \return true w przypadku wystąpienia kolizji
  * \brief Sprawdzanie kolizji między obiektami
  *
  * Metoda sprawdza czy nie zachodzi kolizja między obiektami klas Bullet/Tank
  * oraz Bullet/Stage. Może być użyta dla dowolnego obiektu o ile ma on metody
  * get_collision() oraz get_surface(). W pierwszym kroku fukcja sprawdza
  * czy prostokąty kolizji obiektów mają część wspólną. Jeśli tak to sprawdza
  * czy ta część wspólna jest równa z prostokątem pierwszego obiektu, by nie
  * odczytać danych, do których nie mamy dostępu. Następnie pobierany jest
  * pixel z powierchni drugiego obiektu i sprawdzany jest jego kanał alfa.
  * Jeśli pixel jest przezroczysty kolizja nie występuje. W przeciwnym wypadku
  * kolizja jest/
  */
  template <typename T1, typename T2>
	bool check_collision(T1 *t1, T2 *t2);

  /*!
   * \fn int32* get_pixel(SDL_Surface *surface, int x, int y)
   * \param SDL_Surface* wskaźnik do powierchni przechowującej piksele
   * \param int pozycja x piksela
   * \param int pozycja y piksela
   * \return wskaźnik do 32 bitowej liczby z informacją o pixelu. Format ABGR.
   * \brief Pobranie piksela z powierzchni
   *
   * Piksel jest pobierany jako przesunięcie względem wskaźnika surface->fixels
   * o (x * ilosc_bajtow_na_piksel) + (y * ilosc_pikseli_w_poziomie)
   */
  Uint32* get_pixel(SDL_Surface *surface, int x, int y);

  /*!
   * \fn void destroy_stage(Bullet *b, Stage *s, int r)
   * \param Bullet* wskaźnik do pocisku
   * \param Stage* wskażnik do poziomu
   * \param int promień eksplozji
   * \brief Niszczenie otoczenia
   *
   * Metoda przechodzi po pikselach należacych do kwadratu o boku 2r ze
   * srodkiem w miejscu kolizji tj. współrzędne pocisku. Następnie
   * sprawdza czy współrzędne piksela zawieraję się we współrzędnych
   * powierzchni. Jeśli tak to sprawdzane jest czy dany piksel należy
   * do koła o promieniu r o tym samym środku. W przypadku pozytywnym
   * piksel zostaje nadpisany z kanałem alfa i zaktualizowany na teksturze.
   */
  void destroy_stage(Bullet *b, Stage *s, int r);

public:
  bool ai_turn;                   //!< Informacja czy obecna tura jest turą CPU
  SDL_Event* main_event;          //!< Sktruktura głównego eventu
  bool game_quit;                 //!< Informacja czy gra powinna być zakończona
	bool first_press;               //!< Pierwsze wciśnięcie klawisza na klawiaturze bądź padzie
  short state;                    //!< Stan gry (główne menu, gra)
	bool block_input;               //!< Blokada wejścia na czas lotu pocisku / tury CPU

  Menu *menu;                     //!< Obiekt klasy Menu
  Stage *stage;                   //!< Obiekt klasy Stage
  Tank *tank1;                    //!< Obiekt klasy Tank. Czołg po lewej stronie
  Tank *tank2;                    //!< Obiekt klasy Tank. Czołg po prawej stronie
	Tank *current_tank;             //!< Wskaźnik na czołg, którego tura obowiązuje
  Bullet *bullet;                 //!< Obiekt klasy Bullet
  Explosion *explosion1;          //!< Obiekt klasy Explosion. Eksplozja po kolizji
  Explosion *explosion2;          //!< Obiekt klasy Explosion. Dym z lufy po wystrzale

  /*!
   * \fn Event(SDL_Renderer *_r)
   * \param SDL_Renderer* wskaźnik do struktury renderera umożliwiającej rysowanie
   *
   * \brief Konstruktor klasy Event
   */
  Event(SDL_Renderer *_r);

  /*!
   * \fn ~Event(void)
   * \brief Desktruktor klasy Event
   *
   * Niszczy główny event oraz tekstury i obiekty związane z obiektem klasy.
   */
  ~Event(void);

  /*!
   * \fn void set_turn(void)
   * \brief Zmiana tury
   *
   * Metoda zmienia turę sprawdzając czy przypadniem nie występuje tura CPU
   * oraz wyświetla informacje o turze w górnej części interfejsu
   */
	void set_turn(void);

  /*!
   * \fn void cpu_turn(void)
   * \brief Tura CPU
   *
   * Metoda sprawdza czy obecna tura jest turą CPU. Jeśli tak to castuje
   * obiekt current_tank z Tank* na Ai* oraz wywołuje odpowiednią metodę
   * z klasy Ai
   */
  void cpu_turn(void);

  /*!
   * \fn void draw(void)
   * \brief Główna funkcja rysująca
   *
   * Wywołuje metody draw() obiektów w zależności od stanu gry np: dla stanu
   * gry rysuje pocisk, ekspolozje, czołgi, mapę, interfejs.
   */
  void draw(void);

  /*!
   * \fn bool enter_menu(void)
   * \brief Główna funkcja rysująca
   * \return Informacja czy stan gry powinien zostać zmieniony
   *
   * Obsługa głównego menu po wejściu w daną opcję klawiszem enter
   */
  bool enter_menu(void);

  /*!
   * \fn void return_to_menu(void)
   * \brief Powrót do menu
   *
   * Usunięcie obiektów i zmiana statusu na główne menu
   */
  void return_to_menu(void);

  /*!
   * \fn void shoot(void)
   * \brief Strzał
   *
   * Utworzenie pocisku, eksplozji, zagranie dźwęku i blokada wejścia
   * na czas lotu
   */
  void shoot(void);

  /*!
   * \fn void quit(void)
   * \brief Wyjście z gry
   *
   * Wywołanie eventu SDL_QUIT
   */
  void quit(void);
};

#endif
