#ifndef TANK_HPP
#define TANK_HPP

#include "header.hpp"
#include <cmath>

/*!
 * \enum pos
 * \brief Pozycja czołgu na mapie (lewa, prawa)
 */
enum pos {left, right};

/*!
 * \class Tank
 * \brief Klasa reprezentująca czołg
 */
class Tank{
protected:
  SDL_Point barrel_rotation_POINT;
  SDL_Rect tank_RECT;
  SDL_Rect barrel_RECT;
	SDL_Rect power_RECT;
	SDL_Rect angle_RECT;
	SDL_Rect frame_RECT;
	SDL_Rect frame_fill_RECT;
	SDL_Rect frame_fill2_RECT;
  SDL_Renderer *r;
  SDL_Surface *tmp_SURF;
  SDL_Surface *tank_SURF;
  SDL_Texture *tank_TEX;
  SDL_Texture *barrel_TEX;
	SDL_Texture *power_TEXT;
	SDL_Texture *angle_TEXT;
	SDL_Texture *frame_TEX;
	SDL_Texture *frame_fill_TEX;
	TTF_Font* font;
	SDL_Color color;

  int model;              //!< Model czołgu
  int position;           //!< Pozycja czołgu na mapie
  int barrel_angle;       //!< Kąt pochyłu lufy (6 - 80)
	int power;              //!< Moc strzału (50 - 180)

  /*!
   * \fn void update_ui(void)
   * \brief Aktualizacja wskaźników mocy i kąta w interfejsie
   */
  void update_ui(void);

public:
  const int TANK_WIDTH;     //!< Szerokość czołgu
  const int TANK_HEIGHT;    //!< Wysokość czołgu
  const int BARREL_WIDTH;   //!< Szerokość lufy
  const int BARREL_HEIGHT;  //!< Wysokość lufy

  /*!
   * \fn Tank(SDL_Renderer *_r, int _model, int _position, SDL_Point tank_position)
   * \param SDL_Renderer* wskaźnik do struktury renderera umożliwiającej rysowanie
   * \param int model czołgu
   * \param int pozycja czołgu (lewa, prawa)
   * \param SDL_Point punkt startowy czołgu
   * \brief Konstruktor klasy czołg
   */
  Tank(SDL_Renderer *_r, int _model, int _position, SDL_Point tank_position);

  /*!
   * \fn virtual ~Tank(void)
   * \brief Wirtualny destruktor klasy czołg
   */
	virtual ~Tank(void);

  /*!
   * \fn void barrel_up(void)
   * \brief Zwiększenie kąta nachyłu lufy
   */
  void barrel_up(void);

  /*!
   * \fn void barrel_down(void)
   * \brief Zmniejszenie kąta nachyłu lufy
   */
  void barrel_down(void);

  /*!
   * \fn void power_up(void)
   * \brief Zwiększenie mocy strzału
   */
	void power_up(void);

  /*!
   * \fn void power_down(void)
   * \brief Zmniejszenie mocy strzału
   */
	void power_down(void);

  /*!
   * \fn void draw(void)
   * \brief Rusuje czołg i związany z nim kawałek interfejsu
   */
  void draw(void);

  /*!
   * \fn virtual bool is_ai(void)
   * \return Informacja czy czołgiem steruje komputer. Dla klasy Tank zawsze false
   */
	virtual bool is_ai(void);

  /*!
   * \fn SDL_Texture *get_texture(void)
   * \return wskaźnik do tekstury czołgu
   */
	SDL_Texture *get_texture(void);

  /*!
   * \fn SDL_Rect *get_collision(void)
   * \return Wskaźnik do prostokąta kolizji czołgu
   */
	SDL_Rect *get_collision(void);

  /*!
   * \fn int get_position(void)
   * \return Pozycja czołgu na mapie
   */
	int get_position(void);

  /*!
   * \fn double get_angle(void)
   * \return Kąt nachyłu lufy
   */
	double get_angle(void);

  /*!
   * \fn double get_power(void)
   * \return Moc strzału czołgu
   */
	double get_power(void);

  /*!
   * \fn void set_angle(int a)
   * \param int Kąt
   * \brief Metoda ustawia kąt nachyłu lufy na zadany mieszczący się w limicie
   */
  void set_angle(int a);

  /*!
   * \fn void set_power(int p)
   * \param int moc
   * \brief Metoda ustawia moc na zadaną mieszczącą się w limicie
   */
  void set_power(int p);

  /*!
   * \fn SDL_Point get_barrel_tip(void)
   * \return Punkt wierzchołka lufy
   */
	SDL_Point get_barrel_tip(void);

  /*!
   * \fn SDL_Surface *get_surface(void)
   * \return Wskaźnik do powierzchni wypełnionej pikselami
   */
  SDL_Surface *get_surface(void);
};

#endif //TANK_HPP
