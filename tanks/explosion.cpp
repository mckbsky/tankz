#include "explosion.hpp"

Explosion::Explosion(SDL_Renderer *_r, int type, SDL_Point p) {
  //initializing members
  r = _r;
  start_timer = true;
  i = 0;
  j = 1;
  stop = false;

  //explosion after collision with stage
  if(type == 0)  {
    anim_rows = 3; anim_cols = 8;
    explosion_TEX = Draw::load_texture_from_file(r, "images/explosion0.bmp");
    explosion_part_RECT = {0, 0, 256 / anim_cols, 96 / anim_rows};
    explosion_RECT.w = 88, explosion_RECT.h = 88;
    explosion_RECT.x = p.x - explosion_RECT.w / 2;
    explosion_RECT.y = p.y - explosion_RECT.h / 2;
    interval = 100;
  }
  //explosion after shooting
  else if(type == 1) {
    anim_rows = anim_cols = 6;
    explosion_TEX = Draw::load_texture_from_file(r, "images/explosion1.bmp");
    explosion_part_RECT = {0, 0, 400 / anim_cols, 400 / anim_rows};
    explosion_RECT.w = 50, explosion_RECT.h = 50;
    explosion_RECT.x = p.x - explosion_RECT.w / 2;
    explosion_RECT.y = p.y - explosion_RECT.h / 2;
    interval = 60;
  }
  else {
    std::cerr << "Invalid explosion type" << std::endl;
  }
}

void Explosion::draw(void) {
  if(stop) {
    return;
  }
  Draw::render(r, explosion_TEX, &explosion_part_RECT, &explosion_RECT);

  if(start_timer) {
    time_check = SDL_GetTicks();
    start_timer = false;
  }
  else if(time_check + interval < SDL_GetTicks()) {

    if(i < anim_cols) {
      explosion_part_RECT.x += explosion_part_RECT.w;
      i++;
    }
    else if(i == anim_cols && j < anim_rows) {
      i = 0; j++;
      explosion_part_RECT.y += explosion_part_RECT.h;
    }
    else {
      stop = true;
    }
    start_timer = true;
  }
}

Explosion::~Explosion(void) {
  SDL_DestroyTexture(explosion_TEX);
}
