#include "stage.hpp"

Stage::Stage(SDL_Renderer *_r, int n, bool _wind) {
  //initialize members
  r = _r;
  if(_wind) {
    srand(time(NULL));
    wind = -20 + rand()%40;
  }
  else {
    wind = 0;
  }
  stage_number = n;
  background_RECT.x = 0, background_RECT.y = 0, background_RECT.w = 1920, background_RECT.h = 1080;
	foreground_RECT.x = 0;

  //rectangles for each stage
  if(stage_number == 0) {
    left_tank_position.x = 50, left_tank_position.y = 850;
    right_tank_position.x = 1655, right_tank_position.y = 850;
		foreground_RECT.y = 900, foreground_RECT.w = 1920, foreground_RECT.h = 180;
  }
	else if (stage_number == 1) {
		left_tank_position.x = 50, left_tank_position.y = 940;
		right_tank_position.x = 1655, right_tank_position.y = 920;
		foreground_RECT.y = 370, foreground_RECT.w = 1920, foreground_RECT.h = 710;
	}
  else {
    std::cerr << "Invalid stage number" << std::endl;
  }

  //load textures
  background_TEX = Draw::load_texture_from_file(r, background_RECT, "images/background" + std::to_string(n) + ".bmp");
  stage_SURF = Draw::load_texture_from_file(r, &foreground_TEX, foreground_RECT, "images/foreground" + std::to_string(n) + ".bmp");
}

void Stage::draw() {
  Draw::render(r, background_TEX, &background_RECT);
  Draw::render(r, foreground_TEX, &foreground_RECT);
}

SDL_Point Stage::get_left_tank_position(void) {
  return left_tank_position;
}

SDL_Point Stage::get_right_tank_position(void) {
  return right_tank_position;
}

SDL_Rect* Stage::get_collision(void) {
	return &foreground_RECT;
}

SDL_Texture* Stage::get_texture(void) {
	return foreground_TEX;
}

SDL_Surface* Stage::get_surface(void) {
	return stage_SURF;
}

int Stage::get_wind(void) {
  return wind;
}

Stage::~Stage() {
	SDL_DestroyTexture(foreground_TEX);
	SDL_DestroyTexture(background_TEX);
  SDL_FreeSurface(stage_SURF);
}
