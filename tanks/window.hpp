#ifndef WINDOW_HPP
#define WINDOW_HPP

#include <iostream>
#include <string>
#include "event.hpp"
#include "header.hpp"

class Event;

/*!
 * \class Window
 * \brief Główne okno programu
 */
class Window {
private:
	SDL_Window* main_window;				//!< Główne okno programu
	SDL_DisplayMode current_DM;			//!< Rozdzielczość na pulpicie
	SDL_Renderer* renderer;					//!< Struktura renderera
	Event* event_manager;						//!< Obiekt Event do zarządzania zdarzeniami
	SDL_GameController *pad;				//!< Struktura opisująca gamepad o ile jest podłączony

	int fps;												//!< Aktualny czas klatki
	int time_check;									//!< Licznik czasu w funkcjach związanych z wejściem
	const int MIN_FPS = 16;					//!< Minimalny czas klatki [ms]

	Uint8 dpad_up;									//!< Wartość przycisku dpad_up na gamepadzie
	Uint8 dpad_down;								//!< Wartość przycisku dpad_down na gamepadzie
	Uint8 dpad_left;								//!< Wartość przycisku dpad_left na gamepadzie
	Uint8 dpad_right;								//!< Wartość przycisku dpad_right na gamepadzie
	Uint8 a_button;									//!< Wartość przycisku A na gamepadzie
	Uint8 start_button;							//!< Wartość przycisku start na gamepadzie

public:
	/*!
	 * \fn Window(std::string)
	 * \param std::string Tytuł okna programu
	 *
	 * Konstruktor inicjalizuje bibliotekę SDL oraz jej moduły kontrolera oraz
	 * audio. Następnie tworzy okno programu oraz tworzy renderer z przyspieszeniem
	 * sprzętowym. Następnie tworzy wirtualną powierzchnię o rozmiarze 1920x1080
	 * do której będzie rysował po czym ustawia metodę skalowania rozdzielczośći
	 * na odpowiednia dla zastosowanych assetów. Na końcu konstruktor zajmuje
	 * się otworzeniem mixera audio, wyszukiwania podłączonych gamepadów oraz
	 * tworzy obiekt klasy Event zawierający strukturę SDL_Event.
	 */
	Window(std::string);

	/*!
	 * \fn ~Window(void);
	 * \brief Desktruktor klasy window
	 *
	 * Niszczy renderer, zamyka główne okno, usuwa obiekt Event, zamyka mixer
	 * i wywołuje SDL_Quit();
	 */
	~Window(void);

	/*!
	 * \fn void keyboard_input(void)
	 * \brief obsługa wejścia klawiatury
	 */
	void keyboard_input(void);

	/*!
	 * \fn void gamepad_input(void)
	 * \brief obsługa wejścia gamepada
	 */
	void gamepad_input(void);

	/*!
	 * \fn void main_loop(void)
	 * \brief Główna pętla programu
	 *
	 * Pętla obsługuje zdarzenia związane ze strukturą SDL_Event znajdującą
	 * się w obiekcie event_manager. W każdej klatce czeka ona na zdarzenie
	 * (bez blokowania), czyśli ekran, wywołuje metody do obsługi wejścia,
	 * kopiuje piksele do renderera i wyświetla je na ekran przy pomocy
	 * funkcji SDL_RenderPresent(), Poza tym pętla ogranicza ilość
	 * wyświetlanych klatek na sekundę do 60 wywołując SDL_Delay().
	 */
	void main_loop(void);

};

#endif
