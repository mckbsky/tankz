#include "menu.hpp"

Menu::Menu(SDL_Renderer *_r) {
	r = _r;
	state = title;
  std::fill_n(position, title + 1, 0);
	std::fill_n(new_game_V, 4, 0);
	new_game_V[1] = 1;
	tmp_SURF = nullptr;
	volume = 100;
	ui = 0;
	bgm_MUSIC = Mix_LoadMUS("sounds/menu_bcg.mp3");
	if(!bgm_MUSIC) {
		SDL_Log("%s", Mix_GetError());
	}
	menu_SOUND = Mix_LoadWAV("sounds/menu.wav");

  menu_RECT = {0, 0, 1920, 1080};
	ui_RECT = { 550, 860, 800, 61 };

  background_TEX = Draw::load_texture_from_file(r, "images/menu_bcg.bmp");
	ui1_pad_TEX = Draw::load_texture_from_file(r, ui_RECT, "images/menu_ui1_pad.bmp");
	ui2_pad_TEX = Draw::load_texture_from_file(r, ui_RECT, "images/menu_ui2_pad.bmp");
	ui1_kb_TEX = Draw::load_texture_from_file(r, ui_RECT, "images/menu_ui1_kb.bmp");
	ui2_kb_TEX = Draw::load_texture_from_file(r, ui_RECT, "images/menu_ui2_kb.bmp");
	fill_TEX = Draw::load_texture_from_file(r, "images/selection.bmp");

	//create font
	font = TTF_OpenFont("PixelFJVerdana12pt.ttf", 16);
	if (font == nullptr) {
		SDL_Log("%s", TTF_GetError());
	}
	color = { 0, 0, 0 };
	menu_text_RECT = { BUTTON_X, 220, 0, 0 };
	set_state(title);

	//main menu
	init_submenu_rect(main_menu_RECT, 4);
	main_menu_TEX[0] = set_menu_text("Nowa Gra", main_menu_RECT[0]);
	main_menu_TEX[1] = set_menu_text("Opcje", main_menu_RECT[1]);
	main_menu_TEX[2] = set_menu_text("Autorzy", main_menu_RECT[2]);
	main_menu_TEX[3] = set_menu_text("Wyjscie", main_menu_RECT[3]);

	//new game submenu
	new_game_MAPPED_V[0].push_back("Model 1");
	new_game_MAPPED_V[0].push_back("Model 2");
	new_game_MAPPED_V[1].push_back("Rownina (DEBUG)");
	new_game_MAPPED_V[1].push_back("Wulkan");
	new_game_MAPPED_V[2].push_back("Gracz vs Gracz");
	new_game_MAPPED_V[2].push_back("Gracz vs CPU");
	new_game_MAPPED_V[3].push_back("Wylaczony");
	new_game_MAPPED_V[3].push_back("Wlaczony");

	init_submenu_rect(new_game_RECT, 6);
	new_game_TEX[0] = set_menu_text("Model Czolgu: < " + new_game_MAPPED_V[0][new_game_V[0]] + " >", new_game_RECT[0]);
	new_game_TEX[1] = set_menu_text("Mapa: < " + new_game_MAPPED_V[1][new_game_V[1]] + " >", new_game_RECT[1]);
	new_game_TEX[2] = set_menu_text("Tryb: < " + new_game_MAPPED_V[2][new_game_V[2]] + " >", new_game_RECT[2]);
	new_game_TEX[3] = set_menu_text("Wiatr: < " + new_game_MAPPED_V[3][new_game_V[3]] + " >", new_game_RECT[3]);
	new_game_TEX[4] = set_menu_text("GRAJ", new_game_RECT[4]);
	new_game_TEX[5] = set_menu_text("Wstecz", new_game_RECT[5]);

	//authors submenu
	init_submenu_rect(authors_RECT, 2);
	authors_TEX[0] = set_menu_text("Maciej Brzeczkowski", authors_RECT[0]);
	authors_TEX[1] = set_menu_text("Wstecz", authors_RECT[1]);

	//options submenu
	init_submenu_rect(options_RECT, 2);
	options_TEX[0] = set_menu_text("Glosnosc: < " + std::to_string(volume) + " >", options_RECT[0]);
	options_TEX[1] = set_menu_text("Wstecz", options_RECT[1]);

	//Play some bgm
	Mix_PlayMusic(bgm_MUSIC, -1);
}

void Menu::init_submenu_rect(SDL_Rect *rect, int n) {
	for(int i = 0; i < n; ++i) {
		rect[i] = {BUTTON_X, BUTTON_Y + i * BUTTON_HEIGHT, 0 , 0};
	}
}

SDL_Texture* Menu::set_menu_text(std::string s, SDL_Rect &rect) {
	SDL_Texture *tmp_TEX;
	tmp_SURF = TTF_RenderText_Solid(font, s.c_str(), color);
	tmp_TEX = SDL_CreateTextureFromSurface(r, tmp_SURF);
	SDL_QueryTexture(tmp_TEX, NULL, NULL, &rect.w, &rect.h);
	return tmp_TEX;
}

void Menu::next_item(void) {
	Mix_PlayChannel(-1, menu_SOUND, 0);
	if(state == title) {
		if(position[title] == exit_game) {
			position[title] = new_game;
		}
		else {
			++position[title];
		}
	}
	else if(state == new_game) {
		if(position[new_game] == 5) {
			position[new_game] = 0;
		}
		else {
			++position[new_game];
		}
	}
	else if(state == authors) {
		position[authors] ? position[authors] = 0 : position[authors] = 1;
	}
	else if(state == options) {
		position[options] ? position[options] = 0 : position[options] = 1;
	}
}

void Menu::previous_item(void) {
	Mix_PlayChannel(-1, menu_SOUND, 0);
	if(state == title) {
		if (position[title] == new_game) {
			position[title] = exit_game;
		}
		else {
			--position[title];
		}
	}
	else if(state == new_game) {
		if(position[new_game] == 0) {
			position[new_game] = 5;
		}
		else {
			--position[new_game];
		}
	}
	else if(state == authors) {
		position[authors] ? position[authors] = 0 : position[authors] = 1;
	}
	else if(state == options) {
		position[options] ? position[options] = 0 : position[options] = 1;
	}
}

void Menu::decrease_property(void) {
	if(state == options) {
		if(position[options] == 0 && (volume != 0)) {
			volume -= 10;
			set_volume(volume);
			update_submenu();
		}
	}
	else if(state == new_game && position[new_game] < 4) {
		if(new_game_V[position[new_game]] == 0) {
			new_game_V[position[new_game]] = new_game_MAPPED_V[position[new_game]].size() - 1;
		}
		else {
			--new_game_V[position[new_game]];
		}
		update_submenu();
	}
}


void Menu::update_submenu(void) {
	if(state == new_game) {
		switch(position[new_game]) {
			case 0: {
				new_game_TEX[0] = set_menu_text("Model Czolgu: < " + new_game_MAPPED_V[0][new_game_V[0]] + " >", new_game_RECT[0]);
				break;
			}
			case 1: {
				new_game_TEX[1] = set_menu_text("Mapa: < " + new_game_MAPPED_V[1][new_game_V[1]] + " >", new_game_RECT[1]);
				break;
			}
			case 2: {
				new_game_TEX[2] = set_menu_text("Tryb: < " + new_game_MAPPED_V[2][new_game_V[2]] + " >", new_game_RECT[2]);
				break;
			}
			case 3: {
				new_game_TEX[3] = set_menu_text("Wiatr: < " + new_game_MAPPED_V[3][new_game_V[3]] + " >", new_game_RECT[3]);
			}
		}
	}
	else if(state == options) {
		options_TEX[0] = set_menu_text("Glosnosc: < " + std::to_string(volume) + " >", options_RECT[0]);
	}
}

void Menu::increase_property(void) {
	if(state == options) {
		if(position[options] == 0 && (volume != 100)) {
			volume += 10;
			set_volume(volume);
			update_submenu();
		}
	}
	else if(state == new_game && position[new_game] < 4) {
		if(new_game_V[position[new_game]] == new_game_MAPPED_V[position[new_game]].size() - 1) {
			new_game_V[position[new_game]] = 0;
		}
		else {
			++new_game_V[position[new_game]];
		}
		update_submenu();
	}
}

void Menu::set_volume(int v) {
	Mix_Volume(-1, MIX_MAX_VOLUME * volume / 100.0);
	Mix_VolumeMusic(MIX_MAX_VOLUME * volume / 100.0);
}

short Menu::get_position(void) {
	return position[state];
}

short Menu::get_state(void) {
	return state;
}

int Menu::get_value(int n) {
	return new_game_V[n];
}

void Menu::set_state(short s) {
	state = s;
	if(state == authors) {
		menu_TEXT = set_menu_text("Autorzy", menu_text_RECT);
	}
	else if(state == new_game) {
		menu_TEXT = set_menu_text("Nowa Gra", menu_text_RECT);
	}
	else if(state == title) {
		menu_TEXT = set_menu_text("Menu Glowne", menu_text_RECT);
	}
	else if(state == options) {
		menu_TEXT = set_menu_text("Opcje", menu_text_RECT);
	}
}

void Menu::draw(void) {
  Draw::render(r, background_TEX, &menu_RECT);

	if (state == title) {
		for(int i = 0; i <= 3; ++i) {
			if(position[state] == i) {
				Draw::render_strech(r, fill_TEX, main_menu_RECT[i], BUTTON_WIDTH, BUTTON_HEIGHT);
			}
			Draw::render(r, main_menu_TEX[i], main_menu_RECT[i], BUTTON_WIDTH / 2 - main_menu_RECT[i].w / 2, BUTTON_HEIGHT / 2 - main_menu_RECT[i].h / 2);
		}
	}
	else if(state == new_game) {
		for(int i = 0; i <= 5; ++i) {
			if(position[state] == i) {
				Draw::render_strech(r, fill_TEX, new_game_RECT[i], BUTTON_WIDTH, BUTTON_HEIGHT);
			}
			Draw::render(r, new_game_TEX[i], new_game_RECT[i], BUTTON_WIDTH / 2 - new_game_RECT[i].w / 2, BUTTON_HEIGHT / 2 - new_game_RECT[i].h / 2);
		}
	}
	else if(state == authors) {
		for(int i = 0; i <= 1; ++i) {
			if(position[state] == i) {
				Draw::render_strech(r, fill_TEX, authors_RECT[i], BUTTON_WIDTH, BUTTON_HEIGHT);
			}
			Draw::render(r, authors_TEX[i], authors_RECT[i], BUTTON_WIDTH / 2 - authors_RECT[i].w / 2, BUTTON_HEIGHT / 2 - authors_RECT[i].h / 2);
		}
	}
	else if(state == options) {
		for(int i = 0; i <= 1; ++i) {
			if(position[state] == i) {
				Draw::render_strech(r, fill_TEX, options_RECT[i], BUTTON_WIDTH, BUTTON_HEIGHT);
			}
			Draw::render(r, options_TEX[i], options_RECT[i], BUTTON_WIDTH / 2 - options_RECT[i].w / 2, BUTTON_HEIGHT / 2 - options_RECT[i].h / 2);
		}
	}

	if(state == title) {
		if(ui) {
			Draw::render(r, ui1_pad_TEX, &ui_RECT);
		}
		else {
			Draw::render(r, ui1_kb_TEX, &ui_RECT);
		}
	}
	else {
		if(ui) {
			Draw::render(r, ui2_pad_TEX, &ui_RECT);
		}
		else {
			Draw::render(r, ui2_kb_TEX, &ui_RECT);
		}
	}
	Draw::render(r, menu_TEXT, &menu_text_RECT);

}

Menu::~Menu() {
	for (int i = 0; i < 3; ++i) {
		SDL_DestroyTexture(main_menu_TEX[i]);
		main_menu_TEX[i] = nullptr;
	}
	for(int i = 0; i <= 1; ++i) {
		SDL_DestroyTexture(authors_TEX[i]);
		authors_TEX[i] = nullptr;
	}
	for(int i = 0; i <= 1; ++i) {
		SDL_DestroyTexture(options_TEX[i]);
		options_TEX[i] = nullptr;
	}
	if(tmp_SURF){
		SDL_FreeSurface(tmp_SURF);
	}
	SDL_DestroyTexture(ui1_pad_TEX);
	SDL_DestroyTexture(ui2_pad_TEX);
	SDL_DestroyTexture(menu_TEXT);
}
