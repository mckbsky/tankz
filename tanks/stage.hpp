#ifndef STAGE_HPP
#define STAGE_HPP

#include "header.hpp"
#include <cstdlib>
#include <ctime>
#include <iostream>

/*!
 * \class Stage
 * \brief Klasa reprezentująca poziom
 */
class Stage {
private:
  SDL_Point left_tank_position;
  SDL_Point right_tank_position;
  SDL_Rect background_RECT;
  SDL_Rect foreground_RECT;
  SDL_Renderer *r;
  SDL_Surface *stage_SURF;
  SDL_Texture* background_TEX;
  SDL_Texture* foreground_TEX;
  int wind;           //!< Siła wiatru
  int stage_number;   //!< Numer poziomu

public:
  /*!
   * \fn Stage(SDL_Renderer *_r, int n, bool _wind)
   * \param SDL_Renderer* wskaźnik do struktury renderera umożliwiającej rysowanie
   * \param int numer poziomu
   * \param bool informacja o obecności wiatru
   * \brief Konstruktor klasy Stage
   */
  Stage(SDL_Renderer *_r, int n, bool _wind);

  /*!
   * \fn ~Stage(void)
   * \brief Desktruktor klasy Stage
   */
	~Stage(void);

	/*!
	 * \fn void draw(void)
	 * \brief Rysowanie poziomu
	 *
	 * Metoda rysuje tło i pierwszy plan mapy
	 */
  void draw(void);

	/*!
	 * \fn void draw(void)
   * \return Moc wiatru
	 */
  int get_wind(void);

	/*!
	 * \fn SDL_Rect* get_collision(void)
   * \return Wskaźnik do współrzędnych i rozmiaru planszy
	 */
	SDL_Rect* get_collision(void);

	/*!
	 * \fn SDL_Point get_left_tank_position(void)
   * \return Pozycja lewego czołgu na mapie
	 */
  SDL_Point get_left_tank_position(void);

	/*!
	 * \fn SDL_Point get_right_tank_position(void)
   * \return Pozycja prawego czołgu na mapie
	 */
  SDL_Point get_right_tank_position(void);

	/*!
	 * \fn SDL_Surface *get_surface(void)
   * \return Powierzchnia wypełniona pikselami pierwszego planu
	 */
	SDL_Surface *get_surface(void);

	/*!
	 * \fn SDL_Texture* get_texture(void)
   * \return Tekstura pierwszego planu
	 */
  SDL_Texture* get_texture(void);
};

#endif
