#ifndef DRAW_HPP
#define DRAW_HPP

#include "header.hpp"
#include <string>

/*!
 * \class Draw
 * \brief Klasa związana z ładowaniem tekstur i rysowaniem na ekran
 */

class Draw {
public:
  static SDL_Surface* tmp_SURF;		//!< Tymczasowa powierzchnia
	static const int COLOR_KEY;			//!< Klucz koloru używany do określania koloru przezroczystości (domyślnie 0xff00ff)

	/*!
	* \fn static SDL_Texture* load_texture_from_file(SDL_Renderer *r, std::string filename)
	* \brief Ladowanie tekstur zgodne z SDL 1.2
	*
	* \param SDL_Renderer* wskaźnik do struktury renderera umożliwiającej rysowanie
	* \param std::string nazwa pliku tekstury do załadowania
	* \return SDL_Texture* wskaźnik do załadowanej tekstury
	*
	* Ladowanie tekstur jest wykonywane za pomocą funkcji SDL_CreateTextureFromSurface(), która była
	* wykorzystywana w SDL 1.2. Ladowanie w ten sposób umożliwia dużą swobode w ustawianiu rozmiaru tekstury
	*/
  static SDL_Texture* load_texture_from_file(SDL_Renderer *r, std::string filename);

	/*!
	* \fn static SDL_Texture* load_texture_from_file(SDL_Renderer *r, SDL_Rect rect, std::string filename)
	* \brief Ladowanie tekstur zgodne z SDL 2.0
	*
	* \param SDL_Renderer* wskaźnik do struktury renderera umożliwiającej rysowanie
	* \param SDL_Rect rozmiar tekstury do załadowania
	* \param std::string nazwa pliku tekstury do załadowania
	* \return SDL_Texture* wskaźnik do załadowanej tekstury
	*
	* Ladowanie tekstury z wykorzystaniem SDL_CreateTexture() z SDL 2.0. Troszkę mniejsza swoboda, gdyż musimy
	* podac SDL_Rect, lecz więcej opcji konfiguracji np: formatu pikseli i trybu dostępu
	*/
	static SDL_Texture* load_texture_from_file(SDL_Renderer *r, SDL_Rect rect, std::string filename);

	/*!
	* \fn static SDL_Surface* load_texture_from_file(SDL_Renderer *r, SDL_Texture **t, SDL_Rect rect, std::string filename)
	* \brief Ladowanie tekstur zwracające wskaźnik powierzchni
	*
	* \param SDL_Renderer* wskaźnik do struktury renderera umożliwiającej rysowanie
	* \param SDL_Texture** podwójny wskaźnik na teksturę do załadowania
	* \param SDL_Rect rozmiar tekstury do załadowania
	* \param std::string nazwa pliku tekstury do załadowania
	* \return SDL_Surface* wskaźnik do powierzchni z załadowanymi pikselami
	*
	* Metoda ładuje teksture i zwraca strukturę SDL_Surface*, która zawiera wskaźnik do tablicy zawierającej informacje
	* o poszczególnych pikselach w teksturze. Używana do ładowania tekstur, z którymi związanie są kolizje.
	*/
  static SDL_Surface* load_texture_from_file(SDL_Renderer *r, SDL_Texture **t, SDL_Rect rect, std::string filename);

	/*!
	 * \fn static void render(SDL_Renderer *r, SDL_Texture *t, SDL_Rect *rect)
	 * \brief Rysowanie na ekran
	 *
	 * \param SDL_Renderer* wskaźnik do struktury renderera umożliwiającej rysowanie
	 * \param SDL_Texture* wskaźnik na strukturę do załadowania do renderera
	 * \param SDL_Rect* pozycja i rozmiar tekstury do wyswietlenia
	 *
	 * Kopiuje zawartość tekstury do renderera przy użyciu SDL_RenderCopy()
	 */
  static void render(SDL_Renderer *r, SDL_Texture *t, SDL_Rect *rect);

	/*!
	* \fn static void render(SDL_Renderer *r, SDL_Texture *t, SDL_Rect *rect, int x_offset, int y_offset)
	* \brief Rysowanie na ekran z przesunięciem
	*
	* \param SDL_Renderer* wskaźnik do struktury renderera umożliwiającej rysowanie
	* \param SDL_Texture* wskaźnik na strukturę do załadowania do renderera
	* \param SDL_Rect pozycja i rozmiar tekstury do wyswietlenia
	* \param int Przesunięcie x
	* \param int Przesunięcie y
	*
	* Kopiuje zawartość tekstury do renderera przy użyciu SDL_RenderCopy() po wcześniejszym jej
	* przesunięciu o wartość x,y
	*/
	static void render(SDL_Renderer *r, SDL_Texture *t, SDL_Rect rect, int x_offset, int y_offset);

	/*!
	* \fn static void render(SDL_Renderer *r, SDL_Texture *t, SDL_Rect *rect_src, SDL_Rect *resc_dest)
	* \brief Rysowanie na ekran części tekstury
	*
	* \param SDL_Renderer* wskaźnik do struktury renderera umożliwiającej rysowanie
	* \param SDL_Texture* wskaźnik na strukturę do załadowania do renderera
	* \param SDL_Rect* pozycja i rozmiar całej tekstury
	* \param SDL_Rect* pozycja i rozmiar części tekstury
	*
	* Kopiuje część tekstury do renderera przy użyciu SDL_RenderCopy(). Przydatne do obsługi animacji.
	*/
  static void render(SDL_Renderer *r, SDL_Texture *t, SDL_Rect *rect_src, SDL_Rect *rect_dest);

	/*!
	* \fn static void render_ex(SDL_Renderer *r, SDL_Texture *t, SDL_Rect *rect, SDL_Point point, double angle)
	* \brief Rysowanie na ekran obróconej tekstury
	*
	* \param SDL_Renderer* wskaźnik do struktury renderera umożliwiającej rysowanie
	* \param SDL_Texture* wskaźnik na strukturę do załadowania do renderera
	* \param SDL_Rect* pozycja i rozmiar tekstury
	* \param SDL_Point punkt obrotu
	* \param double kąt obrotu
	*
	* Kopiuje obróconą o kąt względem punktu teksturę do renderera przy użyciu SDL_RenderCopyEx().
	*/
  static void render_ex(SDL_Renderer *r, SDL_Texture *t, SDL_Rect *rect, SDL_Point point, double angle);

	/*!
	* \fn static void render_flipped(SDL_Renderer *r, SDL_Texture *t, SDL_Rect *rect, SDL_Point point, double angle)
	* \brief Rysowanie na ekran obróconej tekstury z lustrzanym odbiciem
	*
	* \param SDL_Renderer* wskaźnik do struktury renderera umożliwiającej rysowanie
	* \param SDL_Texture* wskaźnik na strukturę do załadowania do renderera
	* \param SDL_Rect* pozycja i rozmiar tekstury
	* \param SDL_Point punkt obrotu
	* \param double kąt obrotu
	*
	* Obraca o kąt względem punktu teksturę i wykonuje jej lustrzane odbicie, a następnie kopiuje do renderera przy użyciu SDL_RenderCopyEx().
	*/
  static void render_flipped(SDL_Renderer* r, SDL_Texture* t, SDL_Rect *rect, SDL_Point *point, double angle);

	/*!
	* \fn static void render_strech(SDL_Renderer *r, SDL_Texture *t, SDL_Rect rect, int w, int h)
	* \brief Rysowanie na ekran rozciągniętej tekstury
	*
	* \param SDL_Renderer* wskaźnik do struktury renderera umożliwiającej rysowanie
	* \param SDL_Texture* wskaźnik na strukturę do załadowania do renderera
	* \param SDL_Rect pozycja i rozmiar tekstury
	* \param int szerokość tekstury
	* \param int wysokość tekstury
	*
	* Rozciąga teksturę i wykonuje jej lustrzane odbicie, a następnie kopiuje do renderera przy użyciu SDL_RenderCopy().
	*/
  static void render_strech(SDL_Renderer *r, SDL_Texture *t, SDL_Rect rect, int w, int h);
};

#endif //DRAW_HPP
