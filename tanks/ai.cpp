#include "ai.hpp"

Ai::Ai(SDL_Renderer *_r, int _model, int _position, SDL_Point tank_position, SDL_Point _oppenent_pos)
  :Tank(_r, _model, _position, tank_position) {
  oppenent_pos = _oppenent_pos;
  ai_last_turn = 0;
  ai_first_call = true;
  ai_time_check = SDL_GetTicks();
  collision_pos = {-1, -1};

  srand(time(NULL));
}

void Ai::ai_analize(SDL_Point p) {
  int delta_x = oppenent_pos.x - p.x;
  int delta_y = oppenent_pos.y - p.y;

  if (delta_x > -384 && delta_x < 0 && abs(delta_y) < 216) {
    if(abs(p.x - collision_pos.x) < 10) {
      std::cout << "AI: Undershot small delta, change angle" << std::endl;
      ai_last_turn = 6;
    }
    else {
      std::cout << "AI: Undershot small delta" << std::endl;
      ai_last_turn = 1;
    }
  }
  else if(delta_x < 384 && delta_x > 0 && abs(oppenent_pos.y - p.y) < 216) {
    std::cout << "AI: Overshot small delta" << std::endl;
    ai_last_turn = 2;
  }
  else if(delta_x < -384){
    if(abs(p.x - collision_pos.x) < 130) {
      std::cout << "AI: Undershot - hitting same spot" << std::endl;
      ai_last_turn = 4;
    }
    else {
      std::cout << "AI: Undershot big x delta" << std::endl;
      ai_last_turn = 3;
    }
  }
  else if(abs(delta_x) < 192 && delta_y > 216) {
    std::cout << "AI: Overshot big y delta" << std::endl;
    ai_last_turn = 5;
  }
  else {
    std::cout << "AI: Panic" << std::endl;
    ai_last_turn = 0;
  }
  collision_pos = p;
}

bool Ai::ai_turn(void) {
  if(ai_first_call) {
    ai_time_check = SDL_GetTicks();
    ai_first_call = false;
  }
  else if(ai_time_check + 1200 < SDL_GetTicks()) {
    if(ai_last_turn == 0) {
      //first turn, try shooting with half power + random delta
      set_power(50 + (180 - 50) / 2 + rand()%30);
      set_angle(6 + (80 - 6) / 2 + rand()%15);
    }
    else if(ai_last_turn == 1) {
      //undershot with small delta, increase power
      set_power(power + rand()%15);
    }
    else if(ai_last_turn == 2) {
      //overshot with small delta, decrease power
      set_power(power - rand()%15);
    }
    else if(ai_last_turn == 3) {
      //undershot with big x delta
      set_power(power + rand()%20);
      set_angle(barrel_angle + rand()%3);
    }
    else if(ai_last_turn == 4) {
      //hitting obstacle
      set_power(power + rand()%30);
      set_angle(barrel_angle + rand()%40);
    }
    else if(ai_last_turn == 5) {
      //shot out of map
      set_power(power - rand()%45);
    }
    else if(ai_last_turn == 6) {
      //hitting in the same spot when delta is small
      set_power(power + rand()%15);
      set_angle(barrel_angle - rand()%3);
    }
    ai_first_call = true;
    return true;
  }
  return false;
}

bool Ai::is_ai(void) {
	return true;
}
