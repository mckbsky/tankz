#include "tank.hpp"

Tank::Tank(SDL_Renderer *_r, int _model, int _position, SDL_Point tank_position)
	: TANK_WIDTH(182), TANK_HEIGHT(75), BARREL_WIDTH(81), BARREL_HEIGHT(7) {
	//initialize members
	color = { 255, 255, 255 };
  r = _r;
  model = _model;
  position = _position;
	power = 50; // min = 50, max = 180
  barrel_angle = 10;

	//initialize rectangles
	position == left ? barrel_rotation_POINT.x = 0 : barrel_rotation_POINT.x = BARREL_WIDTH;
	barrel_rotation_POINT.y = 0;
  tank_RECT.x = tank_position.x, tank_RECT.y = tank_position.y, tank_RECT.w = TANK_WIDTH, tank_RECT.h = TANK_HEIGHT;
	position == left ? barrel_RECT.x = tank_RECT.x + 120 : barrel_RECT.x = tank_RECT.x - 12;
	barrel_RECT.y = tank_RECT.y + 10, barrel_RECT.y = tank_RECT.y + 10, barrel_RECT.w = BARREL_WIDTH, barrel_RECT.h = BARREL_HEIGHT;
	position == left ? angle_RECT.x = 15 : angle_RECT.x = 1555;
	angle_RECT.y = 10, angle_RECT.w = 170, angle_RECT.h = 30;
	power_RECT.x = angle_RECT.x, power_RECT.y = 50, power_RECT.w = 170, power_RECT.h = 30;
	frame_RECT.x = angle_RECT.x + angle_RECT.w + 10, frame_RECT.y = angle_RECT.y, frame_RECT.w = angle_RECT.w, frame_RECT.h = angle_RECT.h;
	frame_fill_RECT.x = frame_RECT.x + 3, frame_fill_RECT.y = frame_RECT.y + 3, frame_fill_RECT.h = frame_RECT.h - 6, frame_fill_RECT.w = 0;
	frame_fill2_RECT.x = frame_fill_RECT.x, frame_fill2_RECT.y = frame_fill_RECT.y + frame_fill_RECT.h + 16, frame_fill2_RECT.w = 0, frame_fill2_RECT.h = frame_fill_RECT.h;

	//load textures
  tank_SURF = Draw::load_texture_from_file(r, &tank_TEX, tank_RECT, "images/tank" + std::to_string(model) + ".bmp");
  barrel_TEX = Draw::load_texture_from_file(r, barrel_RECT, "images/barrel" + std::to_string(model) + ".bmp");
	frame_TEX = Draw::load_texture_from_file(r, frame_RECT, "images/frame.bmp");
	frame_fill_TEX = Draw::load_texture_from_file(r, "images/frame_fill.bmp");

	//ui
	font = TTF_OpenFont("PixelFJVerdana12pt.ttf", 13);
	if (font == nullptr) {
		SDL_Log("%s", TTF_GetError());
	}
	tmp_SURF = TTF_RenderText_Solid(font, "Moc strzalu: ", color);
	power_TEXT = SDL_CreateTextureFromSurface(r, tmp_SURF);
	tmp_SURF = TTF_RenderText_Solid(font, "Kat nachylenia: ", color);
	angle_TEXT = SDL_CreateTextureFromSurface(r, tmp_SURF);

	SDL_QueryTexture(power_TEXT, NULL, NULL, &power_RECT.w, &power_RECT.h);
	SDL_QueryTexture(angle_TEXT, NULL, NULL, &angle_RECT.w, &angle_RECT.h);
}

void Tank::barrel_up(void) {
  if(barrel_angle + 2 > 80) {
    return;
  }
  barrel_angle += 2;
	update_ui();
}

void Tank::barrel_down(void) {
  if(barrel_angle - 2 < 6) {
    return;
  }
  barrel_angle -= 2;
	update_ui();
}

void Tank::set_power(int p) {
	if(p < 50 || p > 180) {
		return;
	}
	else {
		power = p;
		update_ui();
	}
}

void Tank::update_ui(void) {
	frame_fill_RECT.w = 2.05 * barrel_angle;
	frame_fill2_RECT.w = 1.09333 * power - 30;
}

void Tank::set_angle(int a) {
	if(a < 6 || a > 80) {
		return;
	}
	else {
		barrel_angle = a;
		update_ui();
	}
}

void Tank::draw() {
  if(position == left) {
    Draw::render(r, tank_TEX, &tank_RECT);
    Draw::render_ex(r, barrel_TEX, &barrel_RECT, barrel_rotation_POINT, barrel_angle);
	}
	else if(position == right) {
		Draw::render_flipped(r, tank_TEX, &tank_RECT, nullptr, 0);
		Draw::render_flipped(r, barrel_TEX, &barrel_RECT, &barrel_rotation_POINT, barrel_angle);

	}
	Draw::render(r, power_TEXT, &power_RECT);
	Draw::render(r, angle_TEXT, &angle_RECT);
	Draw::render(r, frame_TEX, &frame_RECT);
	Draw::render(r, frame_fill_TEX, &frame_fill_RECT);
	Draw::render(r, frame_TEX, frame_RECT, 0, frame_RECT.h + 10);
	Draw::render(r, frame_fill_TEX, &frame_fill2_RECT);
}

SDL_Point Tank::get_barrel_tip(void) {
	SDL_Point tip;
	double angle_RAD = -(barrel_angle) * M_PI / 180;
	if (position == left) {
		tip.x = barrel_RECT.w * cos(angle_RAD) + barrel_RECT.x;
	}
	else {
		tip.x = barrel_RECT.w * cos(angle_RAD + M_PI / 2) + barrel_RECT.x - 20;
	}
	tip.y = barrel_RECT.w * sin(angle_RAD) + barrel_RECT.y;
	return tip;
}

double Tank::get_angle(void) {
	return barrel_angle * -1 * M_PI / 180;
}

double Tank::get_power(void) {
	return power;
}

void Tank::power_up(void) {
	if (power + 4 > 180) {
		return;
	}
	else {
		power += 4;
		update_ui();
	}
}

void Tank::power_down(void) {
	if (power - 4 < 50) {
		return;
	}
	else {
		power -= 4;
	}
	update_ui();
}

int Tank::get_position(void) {
	return position;
}

SDL_Rect* Tank::get_collision(void) {
	return &tank_RECT;
}

SDL_Texture* Tank::get_texture(void) {
	return tank_TEX;
}

SDL_Surface* Tank::get_surface(void) {
	return tank_SURF;
}

bool Tank::is_ai(void) {
	return false;
}

Tank::~Tank() {
	SDL_DestroyTexture(tank_TEX);
	SDL_DestroyTexture(barrel_TEX);
	SDL_DestroyTexture(power_TEXT);
	SDL_DestroyTexture(angle_TEXT);
	SDL_FreeSurface(tmp_SURF);
	SDL_FreeSurface(tank_SURF);
}
