#include "window.hpp"

Window::Window(std::string title) {
	//intialize SDL
	SDL_Init(SDL_INIT_VIDEO | SDL_INIT_GAMECONTROLLER | SDL_INIT_AUDIO);
	if (SDL_GetCurrentDisplayMode(0, &current_DM) != 0) {
		SDL_Log("Could not get display mode for video display: %s", SDL_GetError());
	}
	//create window
	main_window = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, current_DM.w, current_DM.h, SDL_WINDOW_FULLSCREEN);
	renderer = SDL_CreateRenderer(main_window, -1, SDL_RENDERER_ACCELERATED);
	SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);

	//Set window icon
	SDL_Surface *icon_SURF = SDL_LoadBMP("images/icon.bmp");
	SDL_SetWindowIcon(main_window, icon_SURF);

	//Set sane scalling methods for pixelart
	if(SDL_RenderSetLogicalSize(renderer, 1920, 1080) != 0) {
		SDL_Log("%s", SDL_GetError());
	}
	if(SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "0") == SDL_FALSE) {
		SDL_Log("%s", SDL_GetError());
	}

	if(SDL_SetRenderDrawColor(renderer, 0, 0, 0, 70) < 0) {
		SDL_Log("%s", SDL_GetError());
	}

	//Initialize class members
	fps = 0;
	time_check = SDL_GetTicks();

	//Open mixer
	if(Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0) {
		SDL_Log("%s", Mix_GetError());
	}

	//Initialize SDL_ttf
	if(TTF_Init() == -1) {
		SDL_Log("%s", TTF_GetError());
	}

	//Create event manager
	event_manager = new Event(renderer);

	//Check for connected gamepads
	if(SDL_NumJoysticks() > 0) {
		if(SDL_IsGameController(0) == SDL_TRUE) {
			std::cout << "Using game controller: " << SDL_JoystickNameForIndex(0) << std::endl;
			event_manager->menu->ui = 1;
			pad = SDL_GameControllerOpen(0);
		}
	}
	else {
		pad = nullptr;
	}
}

void Window::keyboard_input(void) {
	if (event_manager->main_event->type == SDL_KEYDOWN) {
		if (event_manager->state == main_menu && (!event_manager->first_press || time_check + 150 < SDL_GetTicks())) {
			event_manager->first_press = true;
			switch (event_manager->main_event->key.keysym.sym) {
				case SDLK_ESCAPE: {
					if(event_manager->menu->get_state() == title) {
						event_manager->quit();
						break;
					}
					else {
						event_manager->menu->set_state(title);
					}
				}
				case SDLK_UP: {
					event_manager->menu->previous_item();
					break;
				}
				case SDLK_DOWN: {
					event_manager->menu->next_item();
					break;
				}
				case SDLK_LEFT: {
					event_manager->menu->decrease_property();
					break;
				}
				case SDLK_RIGHT: {
					event_manager->menu->increase_property();
					break;
				}
				case SDLK_RETURN: case SDLK_KP_ENTER: {
					if (event_manager->enter_menu() == true) {
						event_manager->state = game;
					}
					break;
				}
				default: {
					break;
				}
			}
			time_check = SDL_GetTicks();
		}
		else if (event_manager->state == game && !event_manager->block_input &&  (!event_manager->first_press || time_check + 50 < SDL_GetTicks())) {
			event_manager->first_press = true;
			switch (event_manager->main_event->key.keysym.sym) {
				case SDLK_ESCAPE: {
					event_manager->return_to_menu();
					break;
				}
				case SDLK_UP: {
					event_manager->current_tank->barrel_up();
					break;
				}
				case SDLK_DOWN: {
					event_manager->current_tank->barrel_down();
					break;
				}
				case SDLK_SPACE: {
					event_manager->shoot();
					break;
				}
				case SDLK_LEFT: {
					event_manager->current_tank->power_down();
					break;
				}
				case SDLK_RIGHT: {
					event_manager->current_tank->power_up();
					break;
				}
				default: {
					break;
				}
			}
			time_check = SDL_GetTicks();
		}
	}
	else if (event_manager->main_event->type == SDL_KEYUP) {
		event_manager->first_press = false;
	}
}

void Window::gamepad_input(void) {
	if(pad == nullptr) {
		return;
	}

	if(event_manager->main_event->type == SDL_CONTROLLERDEVICEREMOVED) {
		SDL_GameControllerClose(pad);
		pad = nullptr;
		return;
	}

	if (SDL_GameControllerGetAttached(pad)) {
		dpad_up = SDL_GameControllerGetButton(pad, SDL_CONTROLLER_BUTTON_DPAD_UP);
		dpad_down = SDL_GameControllerGetButton(pad, SDL_CONTROLLER_BUTTON_DPAD_DOWN);
		dpad_left = SDL_GameControllerGetButton(pad, SDL_CONTROLLER_BUTTON_DPAD_LEFT);
		dpad_right = SDL_GameControllerGetButton(pad, SDL_CONTROLLER_BUTTON_DPAD_RIGHT);
		a_button = SDL_GameControllerGetButton(pad, SDL_CONTROLLER_BUTTON_A);
		start_button = SDL_GameControllerGetButton(pad, SDL_CONTROLLER_BUTTON_START);
	}
	else {
		return;
	}

	if (event_manager->state == main_menu && (event_manager->first_press == false || time_check + 150 < SDL_GetTicks())) {
		event_manager->first_press = true;
		if(dpad_up) {
			event_manager->menu->previous_item();
		}
		else if (dpad_down) {
			event_manager->menu->next_item();
		}
		else if (dpad_left) {
			event_manager->menu->decrease_property();
		}
		else if (dpad_right) {
			event_manager->menu->increase_property();
		}
		else if (a_button) {
			event_manager->enter_menu();
		}
		else if (start_button) {
			event_manager->quit();
		}
		else {
			event_manager->first_press = false;
		}
		time_check = SDL_GetTicks();
	}
	else if (event_manager->state == game && !event_manager->block_input && (event_manager->first_press == false || time_check + 50 < SDL_GetTicks())) {
		event_manager->first_press = true;
		if (dpad_up) {
			event_manager->current_tank->barrel_up();
		}
		else if (dpad_down) {
			event_manager->current_tank->barrel_down();
		}
		else if (dpad_left) {
			event_manager->current_tank->power_down();
		}
		else if (dpad_right) {
			event_manager->current_tank->power_up();
		}
		else if (a_button) {
			event_manager->shoot();
		}
		else if (start_button) {
			event_manager->return_to_menu();
		}
		else {
			event_manager->first_press = false;
		}
		time_check = SDL_GetTicks();
	}
}

void Window::main_loop() {
	while (!event_manager->game_quit && event_manager->main_event->type != SDL_QUIT) {
		fps = SDL_GetTicks();
		SDL_PollEvent(event_manager->main_event);
		SDL_RenderClear(renderer);

		keyboard_input();
		gamepad_input();
		event_manager->draw();

		event_manager->cpu_turn();

		SDL_RenderPresent(renderer);
		//wait if fps < ~60
		if(SDL_GetTicks() - fps < MIN_FPS) {
			SDL_Delay(MIN_FPS - (SDL_GetTicks () - fps));
		}
	}
}

Window::~Window() {
	if(pad) {
		SDL_GameControllerClose(0);
	}
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(main_window);
	delete event_manager;
	Mix_Quit();
	SDL_Quit();
}
