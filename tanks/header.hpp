#ifndef HEADER_HPP
#define HEADER_HPP

#ifdef _WIN32
//dla Windowsa linkuj statycznie
#include "SDL.h"
#include "SDL_ttf.h"
#include "SDL_gamecontroller.h"
#include "SDL_mixer.h"
#else
//w pozostałych przypadkach dynamicznie
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_gamecontroller.h>
#include <SDL2/SDL_mixer.h>
#endif

#include "draw.hpp"

#endif
