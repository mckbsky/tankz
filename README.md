### <center>Projekt Programowania w Języku C2</center>
---------------------
<center>Politechika Świętokrzyska</center>
<center>Wydział Elektrotechniki, Automatyki i Informatyki</center>
<center>Informatyka rok 2, semestr 3</center>

<center>Autorzy: Maciej Brzęczkowski, Mariusz Lewczuk</center>

#### <center>Czołgi - gra strategiczna dla dwóch graczy</center>
---------------------
Temat projektu to turowa gra, polegająca na takim sterowaniu wartościami mocy i kąta strzału czołgu by zestrzelić swojego oponenta stacjonującego na drugim końcu mapy. Gra pozwala na rozgrykę dla dwóch graczy oraz w trybie gracz konta komputer.

#### <center>Użyty język programowania, biblioteki, IDE, OS</center>
---------------------
Projekt został napisany w języku C++ i tworzony przy użyciu biblioteki SDL w wersji 2.0.5 oraz jej modułów ttf, mixer i gamepad. Użyte systemy operacyjne to Windows 10 i Arch Linux, IDE odpowiednio: Visual C++ oraz Atom z kompilatorem clang.

#### <center>Instrukcja kompilacji/uruchomienia, opis działania i instrukcja obsługi</center>
---------------------
Kompilacja na systemie Windows
> Uruchamiamy plik tanks.sln w Visual Studio i kompilujemy klawiszem F5

Kompilacja na systemie Linux
> * Sprawdzamy czy mamy zainstalowane zależności tj. sdl2, sdl2_mixer, sdl2_ttf
> * Przykładowy sposób instalacji (Arch Linux): 'sudo pacman -S sdl2 sdl2_mixer sdl2_ttf'
> * Przechodzimy do folderu projektu a następnie:
> > cd tanks/  
> > make tanks  
> > ./tanks.out

Po uruchomieniu wybieramy nową grę, wybieramy opcje gry i naciskamy GRAJ.  
Do sterowania mocą i lufą służą strzałki na klawiaturze, spacja służy do strzelania.


#### <center>Zrzuty ekranu z przykładowym zadziałaniem</center>
---------------------
<img src="tanks/ss/ss1.png" alt="ss1" align="center"/>  

<img src="tanks/ss/ss2.png" alt="ss1" align="center"/>

#### <center>Opis użytych algorytmów i najważniejszych fragmentów implementacji</center>
---------------------
##### Bullet::update_trajectory
Obliczanie trajektorii pocisku przy pomocy wzoru na rzut ukośny
```c++
void Bullet::update_trajectory(void) {
	if (position == 0) {
		bullet_RECT.x += interval * t;
		bullet_RECT.y = y0 + (bullet_RECT.x - x0) * tang + (10 * pow(bullet_RECT.x - x0, 2)) / denominator;
	}
	else {
		bullet_RECT.x -= interval * t;
		bullet_RECT.y = y0 - (bullet_RECT.x - x0) * tang + (10 * pow(bullet_RECT.x - x0, 2)) / denominator;
	}
	t += 0.0002;
}
```
##### Event::check_collision
  Metoda sprawdza czy nie zachodzi kolizja między obiektami klas Bullet/Tank
  oraz Bullet/Stage. Może być użyta dla dowolnego obiektu o ile ma on metody
  get_collision() oraz get_surface(). W pierwszym kroku fukcja sprawdza
  czy prostokąty kolizji obiektów mają część wspólną. Jeśli tak to sprawdza
  czy ta część wspólna jest równa z prostokątem pierwszego obiektu, by nie
  odczytać danych, do których nie mamy dostępu. Następnie pobierany jest
  pixel z powierchni drugiego obiektu i sprawdzany jest jego kanał alfa.
  Jeśli pixel jest przezroczysty kolizja nie występuje. W przeciwnym wypadku
  kolizja jest/
```c++
template <typename T1, typename T2>
bool Event::check_collision(T1 *t1, T2 *t2) {
  SDL_Rect result;
	if (SDL_IntersectRect(t1->get_collision(), t2->get_collision(), &result) == SDL_TRUE) {
    if(SDL_RectEquals(t1->get_collision(), &result) == SDL_FALSE) {
      return false;
    }
    else {
      Uint32 pixel = *get_pixel(t2->get_surface(), t1->get_collision()->x, t1->get_collision()->y - t2->get_collision()->y);
      if(pixel == 0xffffff00) {
        return false;
      }
    }
    return true;
	}
	return false;
}
```
##### Event::destroy_stage
Metoda przechodzi po pikselach należacych do kwadratu o boku 2r ze
srodkiem w miejscu kolizji tj. współrzędne pocisku. Następnie
sprawdza czy współrzędne piksela zawieraję się we współrzędnych
powierzchni. Jeśli tak to sprawdzane jest czy dany piksel należy
do koła o promieniu r o tym samym środku. W przypadku pozytywnym
piksel zostaje nadpisany z kanałem alfa i zaktualizowany na teksturze.
```c++
void Event::destroy_stage(Bullet *b, Stage *s, int r) {
  int x = b->get_collision()->x;
  int y = b->get_collision()->y - s->get_collision()->y;

  SDL_LockSurface(s->get_surface());

  for(int i = x - r; i <= x + r; ++i) {
    for(int j = y - r; j <= y + r; ++j) {
      if(i >= 0 && i <= s->get_collision()->w && j >= 0 && j <= s->get_collision()->h) {
        if(pow(r, 2) >= pow(x - i, 2) + pow(y - j, 2)) {
          *get_pixel(s->get_surface(), i, j) = 0xffffff00;
        }
      }
    }
  }

  SDL_UnlockSurface(s->get_surface());
  SDL_UpdateTexture(s->get_texture(), NULL, s->get_surface()->pixels, s->get_collision()->w * sizeof(Uint32));
}
```
#### <center>Podsumowanie</center>
---------------------
Niestety nie udało się zrealizować wszystkich planów związanych z tym projektem.
Pierwszym z nich była kompilacja projektu do systemu Android. Biblioteka SDL jest na tyle przenośna, że niewielkim kosztem można uruchamiać swoje aplikacje na wielu platformach. Kolejnym nie zrealizowanym planem był losowy generator map. SDL2 ma możliwość rysowania kształtów i zapisu do bitmap więc było by to możliwe. Dalszy kierunek rozwoju projektu to szlifowanie sztucznej inteligencji na nowych mapach oraz lepsza obsługa gamepadów (analogi, spusty).
#### <center>Wykorzystane dźwięki na licencji CC</center>
---------------------
http://soundbible.com/1326-Tank-Firing.html  
http://soundbible.com/1472-Depth-Charge.html  
http://opengameart.org/content/dark-fallout  
http://soundbible.com/2072-Shell-Falling.html  
